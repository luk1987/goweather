package main

import (
	"encoding/json"
	"fmt"
	"io/ioutil"
	"net/http"
	"net/url"
	"time"
)

type OPEN_WEATHER_JSON struct {
	Main struct {
		Temp float32
	}
}

type FORECAST_JSON struct {
	Currently struct {
		Temperature float32
	}
}

type LOCATION_JSON struct {
	Results []struct {
		Geometry struct {
			Location struct {
				Lat float32
				Lng float32
			}
		}
	}
}

func kelvin_to_celsius(value float32) float32 {
	return value - 273.15
}

func fahrenheit_to_celsius(value float32) float32 {
	return (value - 32) * 5 / 9
}

func get_url(url string) []byte {
	response, _ := http.Get(url)
	defer response.Body.Close()
	body, _ := ioutil.ReadAll(response.Body)
	return body
}

func get_weather_1(LAT float32, LNG float32, channel chan float32) {
	data := get_url(fmt.Sprintf("http://api.openweathermap.org/data/2.5/weather?lat=%f&lon=%f", LAT, LNG))
	var obj OPEN_WEATHER_JSON
	json.Unmarshal(data, &obj)
	temp := kelvin_to_celsius(obj.Main.Temp)
	channel <- temp
}

func get_weather_2(LAT float32, LNG float32, channel chan float32) {
	data := get_url(fmt.Sprintf("https://api.forecast.io/forecast/9e54e2e8d050ca399c9eacf3840db954/%f,%f", LAT, LNG))
	var obj FORECAST_JSON
	json.Unmarshal(data, &obj)
	temp := fahrenheit_to_celsius(obj.Currently.Temperature)
	channel <- temp
}

func get_location(location string, channel chan LOCATION_JSON) {
	data := get_url("https://maps.googleapis.com/maps/api/geocode/json?address=" + url.QueryEscape(location) + "&sensor=false")
	var obj LOCATION_JSON
	json.Unmarshal(data, &obj)
	channel <- obj
}

func main() {
	start := time.Now()

	c := make(chan LOCATION_JSON)
	go get_location("bacau", c)

	var obj LOCATION_JSON

	obj = <- c

	c1 := make(chan float32)
	c2 := make(chan float32)
	done := make(chan bool)

	lat := obj.Results[0].Geometry.Location.Lat
	lng := obj.Results[0].Geometry.Location.Lng

	fmt.Printf("Weather for %f,%f coords:\n", lat, lng)

	go get_weather_1(lat, lng, c1)
	go get_weather_2(lat, lng, c2)

	go func () {
		for i := 0; i < 2; i++ {
			select {
				case t1 := <- c1:
					fmt.Printf("Weather from openweather.org: %f\n", t1)
				case t2 := <- c2:
					fmt.Printf("Weather from forecast.io: %f\n", t2)
			}
		}
		done <- true
	} ()

	<- done

	duration := time.Since(start)
	fmt.Printf("elapsed time: %f seconds\n", duration.Seconds())
}
